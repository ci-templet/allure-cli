# `GitLab CI` Template: `Allure CLI`

## How to use it

```yml
include:
  - project: 'ci-templet/allure-cli'
    ref: 'main'
    file: 'allure-cli.yml'

allure-cli:
  extends: .allure-cli
  variables:
    REPORT_DIRECTORY: './some/path/to/allure/generated/allure-results'
    REPORT_DIRECTORY: './some/path/to/allure/generated/allure-report'
```

### Example
- [GitLab CI Pipeline](https://gitlab.com/ci-templet/examples/allure-cli/-/blob/master/.gitlab-ci.yml)

### Reference:
- [allure cli](https://github.com/allure-framework/allure2)
